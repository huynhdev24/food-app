/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { Node } from 'react';
import AppMain from './src';

const App: () => Node = () => {
  return <AppMain />;
};

export default App;
