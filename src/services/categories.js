import { api } from '../constants';

const instanceAxios = api.instanceAxios;

export const getCategories = () => {
  return instanceAxios({
    url: '/categories',
    method: 'GET',
  });
};
