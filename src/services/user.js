import { api } from '../constants';

const instanceAxios = api.instanceAxios;

export const getUser = () => {
  return instanceAxios({
    url: '/auth',
    method: 'GET',
    token: true,
  });
};

export const loginGoogle = (data) => {
  return instanceAxios({
    url: '/auth/login-google',
    method: 'POST',
    data,
  });
};

export const updateProfile = (data) => {
  return instanceAxios({
    url: `/users/personal/${data.id}`,
    method: 'PUT',
    data,
    token: true,
  });
};
