import { api } from '../constants';

const instanceAxios = api.instanceAxios;

export const getAllProducts = () => {
  return instanceAxios({
    url: '/products',
    method: 'GET',
  });
};

export const getLatestProducts = () => {
  return instanceAxios({
    url: '/products/latest',
    method: 'GET',
  });
};

export const getTopRatingProducts = () => {
  return instanceAxios({
    url: '/products/top-rating',
    method: 'GET',
  });
};

export const getTopDiscountProducts = () => {
  return instanceAxios({
    url: '/products/top-discount',
    method: 'GET',
  });
};
