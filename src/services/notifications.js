import { api } from '../constants';

const instanceAxios = api.instanceAxios;

export const getUserOrders = () => {
  return instanceAxios({
    url: '/orders/user-orders',
    method: 'GET',
    token: true,
  });
};
