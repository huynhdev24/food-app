import { api } from '../constants';

const instanceAxios = api.instanceAxios;

export const createOrder = (data) => {
  return instanceAxios({
    url: '/orders',
    method: 'POST',
    data,
    token: true,
  });
};
