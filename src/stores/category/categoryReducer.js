import * as categoryActionTypes from './categoryActions';

const initialState = {
  data: [],
};

const categoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case categoryActionTypes.GET_CATEGORIES:
      return {
        ...state,
        data: action.payload.categories,
      };

    default:
      return state;
  }
};

export default categoryReducer;
