import * as api from '../../services/categories';

export const GET_CATEGORIES = 'GET_CATEGORIES';

export const getCategoriesSuccess = (categories) => ({
  type: GET_CATEGORIES,
  payload: { categories },
});
export function getCategories() {
  return (dispatch) => {
    api.getCategories().then((response) => {
      dispatch(getCategoriesSuccess(response?.data.categories));
    });
  };
}
