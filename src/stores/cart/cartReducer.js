import * as cartActionTypes from './cartActions';

const initialState = {
  cart: [],
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case cartActionTypes.LOAD_SUCCESS:
      return {
        ...state,
        cart: action.payload.cart,
      };

    default:
      return state;
  }
};

export default cartReducer;
