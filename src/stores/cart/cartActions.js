import AsyncStorage from '@react-native-async-storage/async-storage';
import { constants } from '../../constants';

export const LOAD_SUCCESS = 'LOAD_SUCCESS';

export const loadSuccess = (cart) => ({
  type: LOAD_SUCCESS,
  payload: { cart },
});

export const loadCart = () => {
  return (dispatch) => {
    AsyncStorage.getItem(constants.ASYNC_STORAGE_CART_NAME).then(
      (productsString) => {
        if (productsString) {
          const cart = JSON.parse(productsString);
          dispatch({
            type: LOAD_SUCCESS,
            payload: { cart },
          });
        } else {
          dispatch({
            type: LOAD_SUCCESS,
            payload: { cart: null },
          });
        }
      }
    );
  };
};

export const addCart = (product, newQuantity) => {
  return (dispatch) => {
    if (parseInt(newQuantity) > parseInt(product?.quantity)) {
      return;
    }
    AsyncStorage.getItem(constants.ASYNC_STORAGE_CART_NAME).then(
      (productsString) => {
        let products = [];
        if (productsString) {
          products = JSON.parse(productsString);
        }
        const productExisted = products.find((item) => {
          if (item.product.id === product.id) {
            if (newQuantity) item.quantity += newQuantity;
            else item.quantity++;
            item.total =
              (item.product.price - item.product.discount) * item.quantity;
            return true;
          }
          return false;
        });
        if (!productExisted) {
          const updateQuantity = newQuantity ?? 1;
          const newProduct = {
            product,
            quantity: updateQuantity,
            total: (product.price - product.discount) * updateQuantity,
          };
          if (newQuantity) products.push(newProduct);
          else products.push(newProduct);
        }
        dispatch({
          type: LOAD_SUCCESS,
          payload: { cart: products },
        });
        AsyncStorage.setItem(
          constants.ASYNC_STORAGE_CART_NAME,
          JSON.stringify(products)
        );
      }
    );
  };
};

export const deleteCart = () => {
  return (dispatch) => {
    AsyncStorage.removeItem(constants.ASYNC_STORAGE_CART_NAME).then(() =>
      dispatch({
        type: LOAD_SUCCESS,
        payload: { cart: null },
      })
    );
  };
};

export const deleteCartItem = (product) => {
  return (dispatch) => {
    AsyncStorage.getItem(constants.ASYNC_STORAGE_CART_NAME).then(
      (productsString) => {
        let products = [];
        if (productsString) {
          products = JSON.parse(productsString);
        }
        const newProduct = products.filter(
          (item) => item.product.id !== product.id
        );
        dispatch({
          type: LOAD_SUCCESS,
          payload: { cart: newProduct },
        });
        AsyncStorage.setItem(
          constants.ASYNC_STORAGE_CART_NAME,
          JSON.stringify(newProduct)
        );
      }
    );
  };
};

export const updateCart = (index, newQuantity, product) => {
  return (dispatch) => {
    AsyncStorage.getItem(constants.ASYNC_STORAGE_CART_NAME).then(
      (productsString) => {
        let products = [];
        if (productsString) {
          products = JSON.parse(productsString);
        }
        if (parseInt(newQuantity) > parseInt(product.quantity)) {
          return;
        }
        products[index].quantity = newQuantity;
        products[index].total =
          (products[index].product.price - products[index].product.discount) *
          products[index].quantity;
        dispatch({
          type: LOAD_SUCCESS,
          payload: { cart: products },
        });
        AsyncStorage.setItem(
          constants.ASYNC_STORAGE_CART_NAME,
          JSON.stringify(products)
        );
      }
    );
  };
};
