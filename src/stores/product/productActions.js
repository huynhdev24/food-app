import * as api from '../../services/products';

export const GET_ALL_PRODUCTS = 'GET_ALL_PRODUCTS';
export const GET_LATEST_PRODUCTS = 'GET_LATEST_PRODUCTS';
export const GET_TOP_RATING_PRODUCTS = 'GET_TOP_RATING_PRODUCTS';
export const GET_TOP_DISCOUNT_PRODUCTS = 'GET_TOP_DISCOUNT_PRODUCTS';

export const getAllProductsSuccess = (allProducts) => ({
  type: GET_ALL_PRODUCTS,
  payload: { allProducts },
});
export function getAllProducts() {
  return (dispatch) => {
    api.getAllProducts().then((response) => {
      dispatch(getAllProductsSuccess(response?.data.products));
    });
  };
}

export const getLatestProductsSuccess = (latestProducts) => ({
  type: GET_LATEST_PRODUCTS,
  payload: { latestProducts },
});
export function getLatestProducts() {
  return (dispatch) => {
    api.getLatestProducts().then((response) => {
      dispatch(getLatestProductsSuccess(response?.data.products));
    });
  };
}

export const getTopRatingProductsSuccess = (topRatingProducts) => ({
  type: GET_TOP_RATING_PRODUCTS,
  payload: { topRatingProducts },
});
export function getTopRatingProducts() {
  return (dispatch) => {
    api.getTopRatingProducts().then((response) => {
      dispatch(getTopRatingProductsSuccess(response?.data.products));
    });
  };
}

export const getTopDiscountProductsSuccess = (topDiscountProducts) => ({
  type: GET_TOP_DISCOUNT_PRODUCTS,
  payload: { topDiscountProducts },
});
export function getTopDiscountProducts() {
  return (dispatch) => {
    api.getTopDiscountProducts().then((response) => {
      dispatch(getTopDiscountProductsSuccess(response?.data.products));
    });
  };
}
