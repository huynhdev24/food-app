import * as productActionTypes from './productActions';

const initialState = {
  allProducts: [],
  latestProducts: [],
  topRatingProducts: [],
  topDiscountProducts: [],
};

const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case productActionTypes.GET_ALL_PRODUCTS:
      return {
        ...state,
        allProducts: action.payload.allProducts,
      };

    case productActionTypes.GET_LATEST_PRODUCTS:
      return {
        ...state,
        latestProducts: action.payload.latestProducts,
      };

    case productActionTypes.GET_TOP_RATING_PRODUCTS:
      return {
        ...state,
        topRatingProducts: action.payload.topRatingProducts,
      };

    case productActionTypes.GET_TOP_DISCOUNT_PRODUCTS:
      return {
        ...state,
        topDiscountProducts: action.payload.topDiscountProducts,
      };

    default:
      return state;
  }
};

export default productReducer;
