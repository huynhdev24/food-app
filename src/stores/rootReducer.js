import { combineReducers } from 'redux';
import tabReducer from './tab/tabReducer';
import productReducer from './product/productReducer';
import categoryReducer from './category/categoryReducer';
import userReducer from './user/userReducer';
import cartReducer from './cart/cartReducer';

export default combineReducers({
  tabReducer,
  productReducer,
  categoryReducer,
  userReducer,
  cartReducer,
});
