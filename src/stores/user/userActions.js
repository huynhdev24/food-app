import AsyncStorage from '@react-native-async-storage/async-storage';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import { constants } from '../../constants';
import * as api from '../../services/user';

export const LOAD_SUCCESS = 'LOAD_SUCCESS';

export const getUserSuccess = (user, isAuthenticated = false) => ({
  type: LOAD_SUCCESS,
  payload: { user, isAuthenticated },
});
export function handleLoadUser() {
  return (dispatch) => {
    AsyncStorage.getItem(constants.ASYNC_STORAGE_TOKEN_NAME).then((value) => {
      if (value !== null) {
        // value previously stored
        api
          .getUser()
          .then((response) => {
            console.log('getUserSuccess', response?.data.user);
            dispatch(getUserSuccess(response?.data.user, true));
          })
          .catch((error) => {
            console.log(error.message);
            AsyncStorage.removeItem(constants.ASYNC_STORAGE_TOKEN_NAME).then(
              () => {
                console.log('getUserFail');
                dispatch(getUserSuccess(null));
              }
            );
          });
      } else {
        dispatch(getUserSuccess(null));
      }
    });
  };
}

export function loginGoogle(data) {
  return (dispatch) => {
    api.loginGoogle(data).then((response) => {
      AsyncStorage.setItem(
        constants.ASYNC_STORAGE_TOKEN_NAME,
        response?.data.accessToken
      ).then(() => {
        console.log('getUser');
        AsyncStorage.getItem(constants.ASYNC_STORAGE_TOKEN_NAME).then(
          (value) => {
            if (value !== null) {
              // value previously stored
              api
                .getUser()
                .then((responseItem) => {
                  console.log('getUserSuccess', responseItem?.data.user);
                  dispatch(getUserSuccess(responseItem?.data.user, true));
                })
                .catch((error) => {
                  console.log(error.message);
                  AsyncStorage.removeItem(
                    constants.ASYNC_STORAGE_TOKEN_NAME
                  ).then(() => {
                    console.log('getUserFail');
                    dispatch(getUserSuccess(null));
                  });
                });
            } else {
              dispatch(getUserSuccess(null));
            }
          }
        );
      });
    });
  };
}

export function logout() {
  return (dispatch) => {
    AsyncStorage.removeItem(constants.ASYNC_STORAGE_TOKEN_NAME).then(() => {
      console.log('logout');
      GoogleSignin.signOut().then(() => dispatch(getUserSuccess(null)));
    });
  };
}
