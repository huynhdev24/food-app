import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { COLORS, FONTS, SIZES } from '../constants';
import { utils } from '../utils';

const HorizontalFoodCard = ({ containerStyle, imageStyle, item, onPress }) => {
  return (
    <TouchableOpacity
      style={{
        flexDirection: 'row',
        borderRadius: SIZES.radius,
        backgroundColor: COLORS.lightGray2,
        ...containerStyle,
      }}
      onPress={onPress}
    >
      {/* Image */}
      {item.images?.length > 0 && (
        <Image source={{ uri: item.images[0].imageUrl }} style={imageStyle} />
      )}

      {/* Info */}
      <View style={{ flex: 1, marginLeft: SIZES.radius }}>
        {/* Name */}
        <Text style={{ ...FONTS.h3, fontSize: 17 }}>{item.name}</Text>

        {/* Description */}
        <Text style={{ ...FONTS.body4, color: COLORS.darkGray2 }}>
          {item.description.substring(0, 30)}...
        </Text>

        {/* Price */}
        <Text style={{ ...FONTS.h2, marginTop: SIZES.base }}>
          &#8363;{utils.convertPrice(item.price)}
        </Text>
      </View>
      {/* Calories */}
      {/* <View
        style={{
          flexDirection: 'row',
          position: 'absolute',
          top: 5,
          right: SIZES.radius,
        }}
      >
        <Image source={icons.calories} style={{ width: 30, height: 30 }} />
        <Text style={{ color: COLORS.darkGray2, ...FONTS.h5 }}>
          {item.calories} Calories
        </Text>
      </View> */}
    </TouchableOpacity>
  );
};

export default HorizontalFoodCard;
