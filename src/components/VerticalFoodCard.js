import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { COLORS, FONTS, icons, SIZES } from '../constants';
import { utils } from '../utils';

const VerticalFoodCard = ({ containerStyle, item, onPress }) => {
  return (
    <TouchableOpacity
      style={{
        width: 200,
        padding: SIZES.radius,
        alignItems: 'center',
        borderRadius: SIZES.radius,
        backgroundColor: COLORS.lightGray2,
        ...containerStyle,
      }}
      onPress={onPress}
    >
      {/* Calories and Favorite */}
      {/* <View style={{ flexDirection: 'row' }}>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <Image source={icons.calories} style={{ width: 30, height: 30 }} />
          <Text style={{ color: COLORS.darkGray2, ...FONTS.body5 }}>
            {item.calories} Calories
          </Text>
        </View>

        <Image
          source={icons.love}
          style={{
            width: 20,
            height: 20,
            tintColor: item.isFavorite ? COLORS.primary : COLORS.gray,
          }}
        />
      </View> */}

      {/* Image */}
      <View
        style={{
          height: 150,
          width: 150,
          alignItems: 'center',
          justifyContent: 'center',
          marginBottom: SIZES.padding * 1.5,
        }}
      >
        {item.images?.length > 0 && (
          <Image
            source={{ uri: item.images[0].imageUrl }}
            style={{
              height: '100%',
              width: '100%',
              borderRadius: SIZES.radius,
            }}
          />
        )}
      </View>

      {/* Info */}
      <View
        style={{
          alignItems: 'center',
          marginTop: -20,
        }}
      >
        <Text style={{ ...FONTS.h3 }}>{item.name}</Text>
        <Text
          style={{
            ...FONTS.body5,
            color: COLORS.darkGray2,
            textAlign: 'center',
          }}
        >
          {item.description.substring(0, 30)}...
        </Text>
        <Text style={{ ...FONTS.h2, marginTop: SIZES.radius }}>
          &#8363;{utils.convertPrice(item.price)}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default VerticalFoodCard;
