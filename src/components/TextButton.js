import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { COLORS, FONTS } from '../constants';

const TextButton = ({
  label,
  labelStyle,
  buttonContainerStyle,
  onPress,
  disabled = false,
  label2 = '',
  label2Style,
}) => {
  return (
    <TouchableOpacity
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: COLORS.primary,
        ...buttonContainerStyle,
      }}
      onPress={onPress}
      disabled={disabled}
    >
      <Text style={{ color: COLORS.white, ...FONTS.h3, ...labelStyle }}>
        {label}
      </Text>

      {label2 !== '' && (
        <Text
          style={{
            flex: 1,
            textAlign: 'right',
            color: COLORS.white,
            ...FONTS.h3,
            label2Style,
          }}
        >
          {label2}
        </Text>
      )}
    </TouchableOpacity>
  );
};

export default TextButton;
