import React from 'react';
import { Text, View } from 'react-native';
import { FONTS } from '../constants';

const Header = ({
  containerStyle,
  title,
  leftComponent,
  rightComponent,
  titleStyle,
}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        ...containerStyle,
      }}
    >
      {/* Lef */}
      {leftComponent}

      {/* Title */}
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text style={{ ...FONTS.h3, ...titleStyle }}>{title}</Text>
      </View>

      {/* Right */}
      {rightComponent}
    </View>
  );
};

export default Header;
