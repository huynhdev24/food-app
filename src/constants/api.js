import initInstanceAxios from './request';

export const apiURL = 'https://yum-yum-backend.onrender.com';

export const instanceAxios = initInstanceAxios(apiURL);

// export const getCategories = (payload) =>
//   axios.get(`${apiURL}/categories`, payload);
// export const createCategory = (payload) =>
//   axios.post(`${apiURL}/categories`, payload);
// export const updateCategory = (payload) =>
//   axios.put(`${apiURL}/categories/${payload.id}`, payload);
// export const deleteCategory = (payload) =>
//   axios.delete(`${apiURL}/categories/${payload}`);
// export const addCategoryImage = (payload) =>
//   axios.put(`${apiURL}/categories/image/${payload.id}`, payload.formData);

// export const getWarehouses = (payload) =>
//   axios.get(`${apiURL}/warehouses`, payload);
// export const createWarehouse = (payload) =>
//   axios.post(`${apiURL}/warehouses`, payload);
// export const updateWarehouse = (payload) =>
//   axios.put(`${apiURL}/warehouses/${payload.id}`, payload);
// export const deleteWarehouse = (payload) =>
//   axios.delete(`${apiURL}/warehouses/${payload}`);

// export const getSuppliers = (payload) =>
//   axios.get(`${apiURL}/suppliers`, payload);
// export const createSupplier = (payload) =>
//   axios.post(`${apiURL}/suppliers`, payload);
// export const updateSupplier = (payload) =>
//   axios.put(`${apiURL}/suppliers/${payload.id}`, payload);
// export const deleteSupplier = (payload) =>
//   axios.delete(`${apiURL}/suppliers/${payload}`);

// export const getProducts = (payload) =>
//   axios.get(`${apiURL}/products`, payload);
// export const createProduct = (payload) =>
//   axios.post(`${apiURL}/products`, payload);
// export const updateProduct = (payload) =>
//   axios.put(`${apiURL}/products/${payload.id}`, payload);
// export const deleteProduct = (payload) =>
//   axios.delete(`${apiURL}/products/${payload}`);
// export const addProductImage = (payload) =>
//   axios.put(`${apiURL}/products/images/${payload.id}`, payload.formData);
// export const deleteProductImage = (payload) =>
//   axios.delete(
//     `${apiURL}/products/images/${payload.id}/delete/${payload.public_id}`,
//     payload
//   );
// export const getLatestProducts = (payload) =>
//   axios.get(`${apiURL}/products/latest`, payload);
// export const getTopRatingProducts = (payload) =>
//   axios.get(`${apiURL}/products/top-rating`, payload);
// export const getTopDiscountProducts = (payload) =>
//   axios.get(`${apiURL}/products/top-discount`, payload);
// export const getRelatedProducts = (payload) =>
//   axios.get(`${apiURL}/products/related/${payload}`);
// export const getProductDetail = (payload) =>
//   axios.get(`${apiURL}/products/${payload}`);

// export const getProductComments = (payload) =>
//   axios.get(`${apiURL}/comments/${payload}`);
// export const createComment = (payload) =>
//   axios.post(`${apiURL}/comments/${payload.id}`, payload);
// export const getAllComments = (payload) =>
//   axios.get(`${apiURL}/comments`, payload);
// export const updateComment = (payload) =>
//   axios.put(`${apiURL}/comments/${payload.id}`, payload);
// export const deleteComment = (payload) =>
//   axios.delete(`${apiURL}/comments/${payload}`);

// export const createOrder = (payload) => axios.post(`${apiURL}/orders`, payload);
// export const getOrders = (payload) => axios.get(`${apiURL}/orders`, payload);
// export const updateOrder = (payload) =>
//   axios.put(`${apiURL}/orders/${payload.id}`, payload);
// export const getUserOrders = (payload) =>
//   axios.get(`${apiURL}/orders/user-orders`, payload);
// export const cancelOrder = (payload) =>
//   axios.put(`${apiURL}/orders/cancel-order/${payload.id}`, payload);
// export const refundStripe = (payload) =>
//   axios.put(`${apiURL}/orders/refund/stripe/${payload.id}`, payload);

// export const getUsers = (payload) => axios.get(`${apiURL}/users/${payload}`);
// export const updateUser = (payload) =>
//   axios.put(`${apiURL}/users/${payload.id}`, payload);
// export const updatePersonal = (payload) =>
//   axios.put(`${apiURL}/users/personal/${payload.id}`, payload);
// export const updateAvatar = (payload) =>
//   axios.put(`${apiURL}/users/image/${payload.id}`, payload.formData);
