// import { store } from '../../redux';

function success(response) {
  /** tùy chỉnh response */

  // const dataResponse = response.data;
  console.log(
    response.status +
      ' - ' +
      response.config.method +
      ' - ' +
      response.config.url
    // dataResponse
  );

  // if (dataResponse?.user && dataResponse?.accessToken) {
  //   /** token auth in login request */
  //   if (dataResponse?.accessToken) {
  //     store.dispatch({
  //       type: 'SET_tokenAuth',
  //       data: dataResponse?.accessToken,
  //     });
  //   }

  //   /** data auth in login request */
  //   if (dataResponse?.user) {
  //     store.dispatch({
  //       type: 'SET_dataCurAuth',
  //       data: dataResponse?.user,
  //     });
  //   }
  // }

  return response;
}

function error(err) {
  //   /** xử lý response, lưu redux nếu cần, sample: */
  //   // const dataResponse = err.response.data
  //   // store.dispatch({ type: 'SET_locate', data: 'en' })
  //   /** xử lý response, lưu redux nếu cần */

  //   /** log result */
  const dataResponse = err.response.data;
  console.log(
    err.response.status +
      ' error - ' +
      err.response.config.method +
      ' - ' +
      err.response.config.url,
    dataResponse
  );
  return new Promise((_resolve, reject) => reject(err));
  //   return new Promise(async (resolve, reject) => {
  //     /** check is token expired */
  //     const result_retry = await checkTokenExpired(err, resolve);
  //     if (result_retry == "LOGOUT") {
  //       logoutSuccess();
  //     } else if (result_retry == "CONFIRM") {
  //       confirmLogoutAuth(false);
  //     }
  //     reject(err);
  //   });
}

export default { success, error };
