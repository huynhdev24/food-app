// import CONSTANTS from '../../constants';
// import { store } from '../../redux';

import AsyncStorage from '@react-native-async-storage/async-storage';
import constants from '../constants';

async function success(config) {
  // const { tokenAuth } = store.getState();
  const tokenAuth = await AsyncStorage.getItem(
    constants.ASYNC_STORAGE_TOKEN_NAME
  );
  const configData = config?.data || {};
  const addToken = config.token;

  /** add token */
  if (addToken === true) {
    config.headers.Authorization = 'Bearer ' + tokenAuth;
  }

  /** add info device */
  // config.headers['x-yumyum-device-token'] = tokenPushNotification;

  /** log */
  if (Object.keys(configData).length) {
    console.log(
      config.method + ' - ' + config.baseURL + config.url,
      configData
    );
  } else {
    console.log(
      config.method + ' - ' + config.baseURL + config.url,
      config.params
    );
  }

  return config;
}

function error(err) {
  return Promise.reject(err);
}

export default { success, error };
