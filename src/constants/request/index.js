import axios from 'axios';
import middlewareRequest from './middlewareRequest';
import middlewareResponse from './middlewareResponse';

const initInstanceAxios = (baseURL) => {
  const instanceAxios = axios.create({
    baseURL: baseURL,
    timeout: 180000,
    headers: {
      'Access-Control-Allow-Origin': '*',
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    method: 'GET',
  });

  /** middleware request */
  instanceAxios.interceptors.request.use(
    middlewareRequest.success,
    middlewareRequest.error
  );

  /** middleware response */
  instanceAxios.interceptors.response.use(
    middlewareResponse.success,
    middlewareResponse.error
  );

  return instanceAxios;
};

export default initInstanceAxios;
