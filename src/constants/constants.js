const onboarding_screens = [
  {
    id: 1,
    backgroundImage: require('../assets/images/background_01.png'),
    bannerImage: require('../assets/images/favourite_food.png'),
    title: 'Chọn một món ăn yêu thích',
    description:
      'Khi bạn đặt hàng tại YumYum, chúng tôi sẽ kết nối bạn với phiếu giảm giá độc quyền, ưu đãi đặc biệt,..',
  },
  {
    id: 2,
    backgroundImage: require('../assets/images/background_02.png'),
    bannerImage: require('../assets/images/hot_delivery.png'),
    title: 'Giao hàng nóng đến tận nhà',
    description:
      'Đặt món ăn nhanh chóng, đơn giản và miễn phí - Bất kể bạn đặt hàng thanh toán trực tuyến hay tiền mặt',
  },
  {
    id: 3,
    backgroundImage: require('../assets/images/background_01.png'),
    bannerImage: require('../assets/images/great_food.png'),
    title: 'Món ăn tuyệt hảo',
    description:
      'Bạn sẽ nhận được món ăn cực ngon trong vòng một giờ. Và được giao hàng miễn phí cho mỗi đơn hàng.',
  },
];

const screens = {
  main_layout: 'MainLayout',
  home: 'Trang Chủ',
  search: 'Tìm Kiếm',
  cart: 'Giỏ Hàng',
  favorite: 'Yêu Thích',
  notification: 'Thông báo',
  my_wallet: 'Ví của tôi',
  profile: 'Thông tin cá nhân',
};

const bottom_tabs = [
  {
    id: 0,
    label: screens.home,
  },
  {
    id: 1,
    label: screens.search,
  },
  {
    id: 2,
    label: screens.cart,
  },
  {
    id: 3,
    label: screens.favorite,
  },
  {
    id: 4,
    label: screens.notification,
  },
  {
    id: 5,
    label: screens.profile,
  },
];

const delivery_time = [
  {
    id: 1,
    label: '10 Phút',
  },
  {
    id: 2,
    label: '20 Phút',
  },
  {
    id: 3,
    label: '30 Phút',
  },
];

const ratings = [
  {
    id: 1,
    label: 1,
  },
  {
    id: 2,
    label: 2,
  },
  {
    id: 3,
    label: 3,
  },
  {
    id: 4,
    label: 4,
  },
  {
    id: 5,
    label: 5,
  },
];

const tags = [
  {
    id: 1,
    label: 'Burger',
  },
  {
    id: 2,
    label: 'Fast Food',
  },
  {
    id: 3,
    label: 'Pizza',
  },
  {
    id: 4,
    label: 'Món Châu Á',
  },
  {
    id: 5,
    label: 'Tráng miệng',
  },
  {
    id: 6,
    label: 'Ăn sáng',
  },
  {
    id: 7,
    label: 'Rau củ',
  },
  {
    id: 8,
    label: 'Taccos',
  },
];

const track_order_status = [
  {
    id: 1,
    title: 'Xác nhận đặt hàng',
    sub_title: 'Đã nhận được đơn đặt hàng của bạn!',
  },
  {
    id: 2,
    title: 'Chuẩn bị đặt hàng',
    sub_title: 'Đơn đặt hàng của bạn đã được chuẩn bị!',
  },
  {
    id: 3,
    title: 'Đang giao hàng',
    sub_title: 'Đợi xíu! Thức ăn của bạn đang trên đường đến!',
  },
  {
    id: 4,
    title: 'Đã giao hàng',
    sub_title: 'Ăn ngon miệng nhé!',
  },
  {
    id: 5,
    title: 'Đánh giá chúng tôi',
    sub_title: 'Giúp chúng tôi cải thiện dịch vụ của mình!',
  },
];

const GOOGLE_MAP_API_KEY = 'AIzaSyCL-kbO3F2880v15g-HEhmB0gAmx3DrB_0';
const ASYNC_STORAGE_TOKEN_NAME = 'token';
const ASYNC_STORAGE_CART_NAME = 'cart';

export default {
  screens,
  bottom_tabs,
  delivery_time,
  ratings,
  tags,
  onboarding_screens,
  track_order_status,
  GOOGLE_MAP_API_KEY,
  ASYNC_STORAGE_TOKEN_NAME,
  ASYNC_STORAGE_CART_NAME,
};
