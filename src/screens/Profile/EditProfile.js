import React, { useState } from 'react';
import { View, Text, Image, StyleSheet, Button } from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { updateProfile } from '../../services/user';
import { handleLoadUser } from '../../stores/user/userActions';
// import { COLORS, dummyData, FONTS, icons, SIZES } from '../../constants';

const EditProfile = ({ navigation, user }) => {
  const [phone, setPhone] = useState(user?.phone);
  const [address, setAddress] = useState(user?.address);

  return (
    <View style={styles.container}>
      <Text style={styles.cancel} onPress={() => navigation.navigate('Home')}>
        Cancel
      </Text>
      <View style={styles.image}>
        <Image
          style={styles.img}
          source={{ uri: user?.avatar?.imageUrl }}
          resizeMode="contain"
        />
        <Image
          style={styles.edit}
          source={require('../../assets/icons/edit.png')}
        />
      </View>
      <View style={styles.info}>
        <View style={styles.text}>
          <Text>Địa chỉ:</Text>
          <TextInput
            style={styles.input}
            value={address}
            onChangeText={(text) => {
              setAddress(text);
            }}
          />
          <Text>Số điện thoại:</Text>
          <TextInput
            style={styles.input}
            value={phone}
            onChangeText={(text) => {
              setPhone(text);
            }}
          />
        </View>
        <View style={styles.button}>
          <TouchableOpacity
            style={styles.btn}
            onPress={async () => {
              if (!phone || !address) {
                return;
              }
              const response = await updateProfile({
                id: user?.id,
                phone,
                address,
                isIgnoreCheck: true,
              });
              if (response.status === 200) {
                handleLoadUser();
                navigation.navigate('Home');
              }
            }}
          >
            <Text style={{ fontSize: 20, color: 'white' }}>Lưu thay đổi</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

function mapStateToProps(state) {
  return {
    isAuthenticated: state.userReducer.isAuthenticated,
    user: state.userReducer.user,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    handleLoadUser: () => {
      return dispatch(handleLoadUser());
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 20,
    marginVertical: 20,
  },
  cancel: {
    color: 'red',
    fontWeight: '300',
    fontSize: 18,
    marginTop: 20,
  },
  image: {
    flex: 1.3,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  info: {
    flex: 2,
  },
  img: {
    height: 230,
    width: 230,
    borderRadius: 100,
    borderWidth: 1,
    borderColor: 'black',
  },
  edit: {
    height: 30,
    width: 30,
    marginTop: 200,
    marginLeft: -30,
  },
  input: {
    height: 50,
    width: '100%',
    borderRadius: 20,
    borderColor: 'black',
    borderWidth: 1,
    marginTop: 10,
    paddingLeft: 20,
    marginBottom: 20,
    backgroundColor: '#f5f8fe',
  },
  text: {
    flex: 1,
    marginTop: 20,
  },
  button: {
    flex: 0.2,
  },
  btn: {
    backgroundColor: '#f16c3d',
    height: 50,
    width: '100%',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
