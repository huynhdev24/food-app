import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, Image, ScrollView } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { COLORS, FONTS } from '../../constants';

const Profile = ({ navigation, user }) => {
  const [profiles, setProfiles] = useState({});

  useEffect(() => {
    getProfile();
  }, []);

  const getProfile = async () => {
    try {
    } catch (error) {}
  };

  return (
    <ScrollView>
      <View
        style={{
          alignItems: 'center',
          marginTop: 10,
          backgroundColor: COLORS.white,
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
        }}
      >
        <View style={styles.headerProfile} />
        <View style={styles.bgProfile}>
          <View>
            <Image
              source={{
                uri:
                  user?.avatar?.imageUrl ||
                  'https://toigingiuvedep.vn/wp-content/uploads/2021/01/avatar-dep-cute.jpg',
              }}
              style={styles.imgProfile}
            />
          </View>
        </View>
        <Text
          style={{
            color: COLORS.black,
            ...FONTS.h3,
            marginVertical: 10,
          }}
        >
          {user?.fullName}
        </Text>
        <TouchableOpacity
          style={styles.btnEdit}
          onPress={() => {
            navigation.navigate('EditProfile');
          }}
        >
          <Text
            style={{
              color: COLORS.white,
              ...FONTS.h4,
            }}
          >
            Chỉnh sửa
          </Text>
        </TouchableOpacity>

        <View style={{ width: '80%' }}>
          <Text
            style={{
              color: COLORS.darkGray,
              ...FONTS.h4,
            }}
          >
            Thông tin nhận hàng
          </Text>
        </View>

        <View style={styles.stats}>
          <View style={styles.stat}>
            <View style={styles.headerStat}>
              <Text
                style={{
                  color: COLORS.darkGray,
                  ...FONTS.h6,
                  marginVertical: 5,
                }}
              >
                Địa chỉ
              </Text>
            </View>

            <Text
              style={{
                color: COLORS.black,
                ...FONTS.h6,
                marginVertical: 5,
              }}
            >
              {user?.address}
            </Text>
          </View>
          <View style={styles.stat}>
            <View style={styles.headerStat}>
              <Text
                style={{
                  color: COLORS.darkGray,
                  ...FONTS.h6,
                  marginVertical: 5,
                }}
              >
                Số điện thoại
              </Text>
            </View>

            <Text
              style={{
                color: COLORS.black,
                ...FONTS.h6,
                marginVertical: 5,
              }}
            >
              {user?.address}
            </Text>
          </View>
          <View style={styles.stat}>
            <View style={styles.headerStat}>
              <Text
                style={{
                  color: COLORS.darkGray,
                  ...FONTS.h6,
                  marginVertical: 5,
                }}
              >
                Email
              </Text>
            </View>

            <Text
              style={{
                color: COLORS.black,
                ...FONTS.h6,
                marginVertical: 5,
              }}
            >
              {user?.email}
            </Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  headerProfile: {
    backgroundColor: COLORS.primary,
    width: '100%',
    height: '25%',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  bgProfile: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.lightGray1,
    width: 150,
    height: 150,
    marginTop: -70,
    borderRadius: 150,
  },
  imgProfile: {
    backgroundColor: 'yellow',
    width: 140,
    height: 140,
    borderRadius: 140,
  },
  btnEdit: {
    backgroundColor: COLORS.primary,
    alignItems: 'center',
    justifyContent: 'center',
    width: 140,
    height: 40,
    marginVertical: 10,
    borderRadius: 10,
  },
  stats: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
    width: '90%',
  },
  stat: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    backgroundColor: COLORS.lightGray2,
    width: '100%',
    borderRadius: 10,
    marginTop: 20,
    paddingLeft: 10,
    paddingBottom: 10,
  },
  headerStat: {
    width: '90%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
});

function mapStateToProps(state) {
  return {
    isAuthenticated: state.userReducer.isAuthenticated,
    user: state.userReducer.user,
  };
}
function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
