import React, { useEffect, useState } from 'react';
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  StatusBar,
  RefreshControl,
  Image,
  FlatList,
} from 'react-native';

// import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { COLORS, dummyData, FONTS, icons, SIZES } from '../../constants';
import { getUserOrders } from '../../services/notifications';

const Notification = () => {
  const [lisOrders, setLisOrders] = useState();
  const [refreshing, setRefreshing] = useState(false);

  const _getUserOrders = async () => {
    const listOrdersResponse = await getUserOrders();
    setLisOrders(listOrdersResponse.data.orders);
  };

  useEffect(() => {
    _getUserOrders();
    return () => {};
  }, []);

  const onRefresh = () => {
    setRefreshing(true);
    _getUserOrders().then(() => {
      setRefreshing(false);
    });
  };

  const Item = ({ title, description, time }) => (
    <View style={styles.item}>
      <Text style={styles.title}>
        <Image
          source={icons.wallet}
          style={{
            height: 20,
            width: 20,
            tintColor: COLORS.red,
          }}
        />
        {'   Đơn hàng '}
        {title}
      </Text>
      <Text style={styles.description}>
        {'Tổng trị giá: ' + description} - {time}
      </Text>
    </View>
  );

  const renderItem = ({ _order }) => (
    <Item
      title={_order?._id}
      description={_order?.totalPrice}
      time={_order?.createdAt}
    />
  );
  console.log('So luong thong bao: ' + lisOrders?.length);
  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 220,
        height: 40,
        marginVertical: SIZES.base,
        paddingHorizontal: SIZES.radius,
        borderRadius: SIZES.radius,
        backgroundColor: COLORS.lightGray2,
      }}
    >
      <SafeAreaView style={styles.container}>
        <ScrollView
          style={styles.scrollView}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        >
          {lisOrders?.length > 0 &&
            lisOrders?.map((_order) => {
              <FlatList
                data={lisOrders}
                renderItem={renderItem}
                keyExtractor={(_order) => _order._id}
              />;
            })}
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
    marginHorizontal: 16,
  },
  scrollView: {
    backgroundColor: '#f5f5f5',
    marginHorizontal: 2,
  },
  text: {
    paddingVertical: 10,
    textAlign: 'center',
    fontWeight: '600',
  },
  item: {
    backgroundColor: '#f5f5f5',
    padding: 20,
    marginVertical: 2,
    borderBottomColor: '#ccc',
    borderBottomWidth: 0.5,
    outlineColor: '#523009',
    outlineStyle: 'solid',
    outlineWidth: 0.5,
  },
  header: {
    fontSize: 32,
    backgroundColor: '#f5f5f5',
  },
  title: {
    color: '#1976d2',
    fontSize: 24,
  },
  description: {
    marginLeft: 20,
    padding: 15,
    color: COLORS.black,
    fontSize: 17,
  },
});

export default Notification;
