import React, { useState } from 'react';
import { Image, View } from 'react-native';
import { FormInput, TextButton } from '../../components';
import { COLORS, icons, SIZES } from '../../constants';
import { utils } from '../../utils';
import AuthLayout from './AuthLayout';

const ForgotPassword = ({ navigation }) => {
  const [email, setEmail] = useState('example@gmail.com');
  const [emailError, setEmailError] = useState('');

  function isEnableSendEmail() {
    return email !== '' && emailError === '';
  }

  return (
    <AuthLayout
      title="Khôi phục mật khẩu"
      subtitle="Vui lòng nhập địa chỉ email của bạn để lấy lại mật khẩu"
      titleContainerStyle={{
        marginTop: SIZES.padding * 2,
      }}
    >
      {/* Form Input */}
      <View style={{ flex: 1, marginTop: SIZES.padding * 2 }}>
        <FormInput
          label="Email"
          keyboardType="Địa chỉ email"
          autoCompleteType="email"
          onChange={(value) => {
            utils.validateEmail(value, setEmailError);
            setEmail(value);
          }}
          value={email}
          errorMsg={emailError}
          appendComponent={
            <View
              style={{
                justifyContent: 'center',
              }}
            >
              <Image
                source={
                  email === '' || (email !== '' && emailError === '')
                    ? icons.correct
                    : icons.cancel
                }
                style={{
                  height: 20,
                  width: 20,
                  tintColor:
                    email === ''
                      ? COLORS.gray
                      : email !== '' && emailError === ''
                      ? COLORS.green
                      : COLORS.red,
                }}
              />
            </View>
          }
        />
      </View>

      {/* Button */}
      <TextButton
        disabled={!isEnableSendEmail()}
        label="Gửi Email"
        buttonContainerStyle={{
          height: 50,
          alignItems: 'center',
          marginTop: SIZES.padding,
          borderRadius: SIZES.radius,
          backgroundColor: isEnableSendEmail()
            ? COLORS.primary
            : COLORS.transparentPrimary,
        }}
        onPress={() => {
          navigation.goBack();
        }}
      />
    </AuthLayout>
  );
};

export default ForgotPassword;
