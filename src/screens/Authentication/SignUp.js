import React, { useState } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { FormInput, TextButton, TextIconButton } from '../../components';
import { COLORS, FONTS, icons, SIZES } from '../../constants';
import { utils } from '../../utils';
import AuthLayout from './AuthLayout';

const SignUp = ({ navigation }) => {
  const [email, setEmail] = useState('example@gmail.com');
  const [username, setUsername] = useState('example');
  const [password, setPassword] = useState('123456789');
  const [showPassword, setShowPassword] = useState(false);
  const [emailError, setEmailError] = useState('');
  // const [usernameError, setUsernameError] = useState('');
  const [passwordError, setPasswordError] = useState('');

  function isEnableSignUp() {
    return (
      email !== '' &&
      username !== '' &&
      password !== '' &&
      emailError === '' &&
      passwordError === ''
      // && usernameError === ''
    );
  }

  return (
    <AuthLayout
      title="Bắt đầu"
      subtitle="Tạo một tài khoản để tiếp tục"
      titleContainerStyle={{
        marginTop: SIZES.radius,
      }}
    >
      <View
        style={{
          flex: 1,
          marginTop: SIZES.padding + 2,
        }}
      >
        {/* Form Input & Sign Up */}
        <FormInput
          label="Email"
          keyboardType="Địa chỉ email"
          autoCompleteType="email"
          onChange={(value) => {
            utils.validateEmail(value, setEmailError);
            setEmail(value);
          }}
          value={email}
          errorMsg={emailError}
          appendComponent={
            <View
              style={{
                justifyContent: 'center',
              }}
            >
              <Image
                source={
                  email === '' || (email !== '' && emailError === '')
                    ? icons.correct
                    : icons.cancel
                }
                style={{
                  height: 20,
                  width: 20,
                  tintColor:
                    email === ''
                      ? COLORS.gray
                      : email !== '' && emailError === ''
                      ? COLORS.green
                      : COLORS.red,
                }}
              />
            </View>
          }
        />

        <FormInput
          label="Tài khoản"
          containerStyle={{ marginTop: SIZES.radius }}
          onChange={(value) => {
            setUsername(value);
          }}
          value={username}
          // errorMsg={usernameError}
          appendComponent={
            <View
              style={{
                justifyContent: 'center',
              }}
            >
              <Image
                source={
                  username === '' || username !== ''
                    ? // && usernameError === '')
                      icons.correct
                    : icons.cancel
                }
                style={{
                  height: 20,
                  width: 20,
                  tintColor:
                    username === ''
                      ? COLORS.gray
                      : username !== ''
                      ? // && usernameError === ''
                        COLORS.green
                      : COLORS.red,
                }}
              />
            </View>
          }
        />

        <FormInput
          label="Mật khẩu"
          secureTextEntry={!showPassword}
          autoCompleteType="password"
          containerStyle={{
            marginTop: SIZES.radius,
          }}
          onChange={(value) => {
            utils.validatePassword(value, setPasswordError);
            setPassword(value);
          }}
          value={password}
          errorMsg={passwordError}
          appendComponent={
            <TouchableOpacity
              style={{
                width: 40,
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}
              onPress={() => {
                setShowPassword(!showPassword);
              }}
            >
              <Image
                source={showPassword ? icons.eye_close : icons.eye}
                style={{
                  height: 20,
                  width: 20,
                  tintColor: COLORS.green,
                }}
              />
            </TouchableOpacity>
          }
        />

        {/* Sign Up */}
        <TextButton
          label="Đăng ký"
          disabled={!isEnableSignUp()}
          buttonContainerStyle={{
            height: 55,
            alignItems: 'center',
            marginTop: SIZES.padding,
            borderRadius: SIZES.radius,
            backgroundColor: isEnableSignUp()
              ? COLORS.primary
              : COLORS.transparentPrimary,
          }}
          onPress={() => {
            navigation.replace('Otp');
          }}
        />

        {/* Sign In */}
        <View
          style={{
            flexDirection: 'row',
            marginTop: SIZES.radius,
            justifyContent: 'center',
          }}
        >
          <Text style={{ color: COLORS.darkGray, ...FONTS.body3 }}>
            Already have an account?
          </Text>

          <TextButton
            label="Đăng nhập"
            buttonContainerStyle={{
              backgroundColor: null,
            }}
            labelStyle={{
              color: COLORS.primary,
              ...FONTS.h3,
            }}
            onPress={() => {
              navigation.replace('SignIn');
            }}
          />
        </View>
      </View>

      {/* Footer */}
      <View>
        {/* Facebook */}
        <TextIconButton
          containerStyle={{
            height: 50,
            alignItems: 'center',
            borderRadius: SIZES.radius,
            backgroundColor: COLORS.blue,
          }}
          icon={icons.fb}
          iconPosition="LEFT"
          iconStyle={{
            tintColor: COLORS.white,
          }}
          label="Tiếp tục với Facebook"
          labelStyle={{
            marginLeft: SIZES.radius,
            color: COLORS.white,
          }}
          onPress={() => {
            console.log('Facebook');
          }}
        />

        {/* Google */}
        <TextIconButton
          containerStyle={{
            height: 50,
            alignItems: 'center',
            marginTop: SIZES.radius,
            borderRadius: SIZES.radius,
            backgroundColor: COLORS.lightGray2,
          }}
          icon={icons.google}
          iconPosition="LEFT"
          iconStyle={{
            tintColor: null,
          }}
          label="Tiếp tục với Google"
          labelStyle={{
            marginLeft: SIZES.radius,
          }}
          onPress={() => {
            console.log('Google');
          }}
        />
      </View>
    </AuthLayout>
  );
};

export default SignUp;
