import React, { useEffect, useState } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { FormInput, TextButton, TextIconButton } from '../../components';
import CustomSwitch from '../../components/CustomSwitch';
import { COLORS, FONTS, icons, SIZES } from '../../constants';
import { utils } from '../../utils';
import AuthLayout from './AuthLayout';
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import { connect } from 'react-redux';
import { loginGoogle } from '../../stores/user/userActions';

const SignIn = ({ navigation, loginGoogle, isAuthenticated }) => {
  const [email, setEmail] = useState('example@gmail.com');
  const [password, setPassword] = useState('123456789');
  const [emailError, setEmailError] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const [saveMe, setSaveMe] = useState(false);

  useEffect(() => {
    if (isAuthenticated) {
      navigation.replace('Home');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAuthenticated]);

  useEffect(() => {
    GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true });
    GoogleSignin.configure({
      webClientId: '',
      iosClientId: '',
      offlineAccess: true,
    });
  }, []);

  function isEnableSignIn() {
    return email !== '' && password !== '' && emailError === '';
  }

  const handleSignin = async () => {
    try {
      const userInfo = await GoogleSignin.signIn();
      loginGoogle({
        email: userInfo.user.email,
        name: userInfo.user.name,
        photo: userInfo.user.photo,
      });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        console.log('user cancelled the login flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
        console.log('in progress');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        console.log('play services not available or outdated');
      } else {
        // some other error happened
        console.log(error);
      }
    }
  };

  return (
    <AuthLayout title="Hãy đăng nhập" subtitle="Chào mừng trở lại!">
      {/* <View
        style={{
          flex: 1,
          marginTop: SIZES.padding + 2,
        }}
      >
        <FormInput
          label="Email"
          keyboardType="email-address"
          autoCompleteType="email"
          onChange={(value) => {
            utils.validateEmail(value, setEmailError);
            setEmail(value);
          }}
          value={email}
          errorMsg={emailError}
          appendComponent={
            <View
              style={{
                justifyContent: 'center',
              }}
            >
              <Image
                source={
                  email === '' || (email !== '' && emailError === '')
                    ? icons.correct
                    : icons.cancel
                }
                style={{
                  height: 20,
                  width: 20,
                  tintColor:
                    email === ''
                      ? COLORS.gray
                      : email !== '' && emailError === ''
                      ? COLORS.green
                      : COLORS.red,
                }}
              />
            </View>
          }
        />
        <FormInput
          label="Password"
          secureTextEntry={!showPassword}
          autoCompleteType="password"
          containerStyle={{
            marginTop: SIZES.radius,
          }}
          onChange={(value) => {
            setPassword(value);
          }}
          value={email}
          appendComponent={
            <TouchableOpacity
              style={{
                width: 40,
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}
              onPress={() => {
                setShowPassword(!showPassword);
              }}
            >
              <Image
                source={showPassword ? icons.eye_close : icons.eye}
                style={{
                  height: 20,
                  width: 20,
                  tintColor: COLORS.green,
                }}
              />
            </TouchableOpacity>
          }
        />

        <View
          style={{
            flexDirection: 'row',
            marginTop: SIZES.radius,
            justifyContent: 'space-between',
          }}
        >
          <CustomSwitch
            value={saveMe}
            onChange={(value) => {
              setSaveMe(value);
            }}
          />

          <TextButton
            label="Forgot Password"
            buttonContainerStyle={{
              backgroundColor: null,
            }}
            labelStyle={{
              color: COLORS.gray,
              ...FONTS.body4,
            }}
            onPress={() => {
              navigation.navigate('ForgotPassword');
            }}
          />
        </View>

s        <TextButton
          label="Sign In"
          disabled={!isEnableSignIn()}
          buttonContainerStyle={{
            height: 55,
            alignItems: 'center',
            marginTop: SIZES.padding,
            borderRadius: SIZES.radius,
            backgroundColor: isEnableSignIn()
              ? COLORS.primary
              : COLORS.transparentPrimary,
          }}
          onPress={() => {
            navigation.replace('Home');
          }}
        />

        <View
          style={{
            flexDirection: 'row',
            marginTop: SIZES.radius,
            justifyContent: 'center',
          }}
        >
          <Text style={{ color: COLORS.darkGray, ...FONTS.body3 }}>
            Don't have an account?
          </Text>

          <TextButton
            label="Sign Up"
            buttonContainerStyle={{
              marginLeft: 3,
              backgroundColor: null,
            }}
            labelStyle={{
              color: COLORS.primary,
              ...FONTS.h3,
            }}
            onPress={() => {
              navigation.replace('SignUp');
            }}
          />
        </View>
      </View> */}

      {/* Footer */}
      <View>
        {/* Facebook */}
        {/* <TextIconButton
          containerStyle={{
            height: 50,
            alignItems: 'center',
            borderRadius: SIZES.radius,
            backgroundColor: COLORS.blue,
          }}
          icon={icons.fb}
          iconPosition="LEFT"
          iconStyle={{
            tintColor: COLORS.white,
          }}
          label="Continue with Facebook"
          labelStyle={{
            marginLeft: SIZES.radius,
            color: COLORS.white,
          }}
          onPress={() => {
            console.log('Facebook');
          }}
        /> */}

        {/* Google */}
        <TextIconButton
          containerStyle={{
            height: 50,
            alignItems: 'center',
            marginTop: SIZES.radius,
            borderRadius: SIZES.radius,
            backgroundColor: COLORS.lightGray2,
          }}
          icon={icons.google}
          iconPosition="LEFT"
          iconStyle={{
            tintColor: null,
          }}
          label="Tiếp tục với Google"
          labelStyle={{
            marginLeft: SIZES.radius,
          }}
          onPress={handleSignin}
        />
      </View>
    </AuthLayout>
  );
};

function mapStateToProps(state) {
  return {
    isAuthenticated: state.userReducer.isAuthenticated,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    loginGoogle: (data) => {
      return dispatch(loginGoogle(data));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
