import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  Platform,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import { HorizontalFoodCard, VerticalFoodCard } from '../../components';
import { COLORS, FONTS, icons, SIZES } from '../../constants';
import { loadCart } from '../../stores/cart/cartActions';
import { getCategories } from '../../stores/category/categoryActions';
import {
  getAllProducts,
  getLatestProducts,
  getTopDiscountProducts,
  getTopRatingProducts,
} from '../../stores/product/productActions';
import { handleLoadUser } from '../../stores/user/userActions';
import FilterModal from './FilterModal';

const Section = ({ title, onPress, children }) => {
  return (
    <View>
      {/* Header */}
      <View>
        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: SIZES.padding,
            marginTop: 30,
            marginBottom: 20,
          }}
        >
          <Text style={{ flex: 1, ...FONTS.h3 }}>{title}</Text>

          {/* <TouchableOpacity onPress={onPress}>
            <Text style={{ color: COLORS.primary, ...FONTS.body3 }}>
              Show All
            </Text>
          </TouchableOpacity> */}
        </View>
      </View>

      {/* Content */}
      {children}
    </View>
  );
};

const Home = ({
  navigation,
  categories,
  allProducts,
  latestProducts,
  topRatingProducts,
  topDiscountProducts,
  getCategories,
  getAllProducts,
  getLatestProducts,
  getTopRatingProducts,
  getTopDiscountProducts,
  handleLoadUser,
  isAuthenticated,
  user,
  loadCart,
}) => {
  const [selectedCategoryId, setSelectedCategoryId] = useState(
    '627333d7b297b1658b99d770'
  );
  const [selectedMenuType, setSelectedMenuType] = useState(1);
  const [menuList, setMenuList] = useState([]);
  const [recommends, setRecommends] = useState([]);
  const [popular, setPopular] = useState([]);
  const [showFilterModal, setShowFilterModal] = useState(false);
  const [searchText, setSearchText] = useState('');
  const menu = [
    {
      id: 1,
      name: 'Đặc sắc',
    },
    {
      id: 2,
      name: 'Phổ biến',
    },
    {
      id: 3,
      name: 'Mới nhất',
    },
    {
      id: 4,
      name: 'Được đề xuất',
    },
  ];

  useEffect(() => {
    if (categories?.length === 0) {
      getCategories();
    }
    if (allProducts?.length === 0) {
      getAllProducts();
    }
    if (latestProducts?.length === 0) {
      getLatestProducts();
    }
    if (topRatingProducts?.length === 0) {
      getTopRatingProducts();
    }
    if (topDiscountProducts?.length === 0) {
      getTopDiscountProducts();
    }

    loadCart();
    handleLoadUser();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    handleChangeCategory(selectedCategoryId, selectedMenuType);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    categories?.length,
    allProducts?.length,
    latestProducts?.length,
    topRatingProducts?.length,
    topDiscountProducts?.length,
  ]);

  function handleSearch(name) {
    return name
      .toLowerCase()
      .normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '')
      .includes(
        searchText
          .toLowerCase()
          .normalize('NFD')
          .replace(/[\u0300-\u036f]/g, '')
      );
  }

  function renderSearch() {
    return (
      <View
        style={{
          flexDirection: 'row',
          height: 40,
          alignItems: 'center',
          marginHorizontal: SIZES.padding,
          marginVertical: SIZES.base,
          paddingHorizontal: SIZES.radius,
          borderRadius: SIZES.radius,
          backgroundColor: COLORS.lightGray2,
        }}
      >
        {/* Icon */}
        <Image
          source={icons.search}
          style={{
            height: 20,
            width: 20,
            tintColor: COLORS.black,
          }}
        />

        {/* Text Input */}
        <TextInput
          style={{
            flex: 1,
            marginLeft: SIZES.radius,
            ...FONTS.body3,
          }}
          placeholder={Platform.OS === 'ios' ? 'Tìm kiếm...' : ''}
          onChangeText={(text) => {
            setSearchText(text);
            handleChangeCategory(
              selectedCategoryId,
              selectedMenuType,
              searchText
            );
          }}
          value={searchText}
        />

        {/* Filter Button */}
        {/* <TouchableOpacity
          onPress={() => {
            setShowFilterModal(true);
          }}
        >
          <Image
            source={icons.filter}
            style={{ height: 20, width: 20, tintColor: COLORS.black }}
          />
        </TouchableOpacity> */}
      </View>
    );
  }

  function renderDeliveryTo() {
    return (
      <View
        style={{
          marginTop: SIZES.padding,
          marginHorizontal: SIZES.padding,
        }}
      >
        <Text style={{ color: COLORS.primary, ...FONTS.body3 }}>
          Địa chỉ nhận hàng
        </Text>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            marginTop: SIZES.base,
            alignItems: 'center',
          }}
          onPress={() => {
            if (!isAuthenticated) {
              navigation.navigate('SignIn');
            } else {
              navigation.navigate('EditProfile');
            }
          }}
        >
          <Text style={{ ...FONTS.h3 }}>
            {user?.address || 'Vui lòng cập nhật địa chỉ'}
          </Text>
          <Image
            source={icons.down_arrow}
            style={{
              marginLeft: SIZES.base,
              height: 20,
              width: 20,
            }}
          />
        </TouchableOpacity>
      </View>
    );
  }

  function handleChangeCategory(categoryId, menuTypeId, text = '') {
    // retrieve the popular menu
    let selectedPopular = topRatingProducts;

    // retrieve the recommended menu
    let selectedRecommend = topDiscountProducts;

    // find the menu based on menuTypeId
    let selectedMenu;
    switch (menuTypeId) {
      case 1:
        selectedMenu = allProducts;
        break;

      case 2:
        selectedMenu = topRatingProducts;
        break;

      case 3:
        selectedMenu = latestProducts;
        break;

      case 4:
        selectedMenu = topDiscountProducts;
        break;

      default:
        break;
    }

    // set the popular menu based on the categoryId
    setPopular(
      selectedPopular?.filter(
        (item) => item.categoryId === categoryId && handleSearch(item.name)
      )
    );

    // set the recommended menu based on the categoryId
    setRecommends(
      selectedRecommend?.filter(
        (item) => item.categoryId === categoryId && handleSearch(item.name)
      )
    );

    // set the menu based on the categoryId
    setMenuList(
      selectedMenu?.filter(
        (item) =>
          (item.categoryId === categoryId ||
            item.categoryId?.id === categoryId) &&
          handleSearch(item.name)
      )
    );
  }

  function renderFoodCategories() {
    return (
      <FlatList
        data={categories}
        keyExtractor={(item) => `${item.id}`}
        horizontal
        showsHorizontalScrollIndicator={false}
        renderItem={({ item, index }) => (
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              height: 55,
              marginTop: SIZES.padding,
              marginLeft: index === 0 ? SIZES.padding : SIZES.radius,
              marginRight: index === categories.length - 1 ? SIZES.padding : 0,
              paddingHorizontal: 8,
              borderRadius: SIZES.radius,
              backgroundColor:
                selectedCategoryId === item.id
                  ? COLORS.primary
                  : COLORS.lightGray2,
            }}
            onPress={() => {
              setSelectedCategoryId(item.id);
              handleChangeCategory(item.id, selectedMenuType);
            }}
          >
            <Image
              source={{ uri: item.image.imageUrl }}
              style={{
                height: 50,
                width: 50,
                marginTop: 5,
                borderRadius: SIZES.radius,
              }}
            />
            <Text
              style={{
                alignSelf: 'center',
                marginRight: SIZES.base,
                marginLeft: SIZES.base,
                color:
                  selectedCategoryId === item.id
                    ? COLORS.white
                    : COLORS.darkGray,
              }}
            >
              {item.name}
            </Text>
          </TouchableOpacity>
        )}
      />
    );
  }

  function renderRecommendedSection() {
    return (
      <Section
        title="Được đề xuất"
        onPress={() => {
          console.log('Hiển thị tất cả đề xuất');
        }}
      >
        <FlatList
          data={recommends}
          keyExtractor={(item) => `${item.id}`}
          horizontal
          showsHorizontalScrollIndicator={false}
          renderItem={({ item, index }) => (
            <HorizontalFoodCard
              containerStyle={{
                height: 180,
                width: SIZES.width * 0.85,
                marginLeft: index === 0 ? SIZES.padding : 18,
                marginRight:
                  index === recommends.length - 1 ? SIZES.padding : 0,
                paddingRight: SIZES.radius,
                alignItems: 'center',
              }}
              imageStyle={{
                height: 150,
                width: 150,
                borderRadius: SIZES.radius,
                marginLeft: SIZES.radius,
              }}
              item={item}
              onPress={() => {
                console.log('HorizontalFoodCard');
                navigation.navigate('FoodDetail', { food: item });
              }}
            />
          )}
        />
      </Section>
    );
  }

  function renderPopularSection() {
    return (
      <Section
        title="Phổ biến gần bạn"
        onPress={() => {
          console.log('Hiển thị tất cả các mặt hàng phổ biến');
        }}
      >
        <FlatList
          data={popular}
          keyExtractor={(item) => `${item.id}`}
          horizontal
          showsHorizontalScrollIndicator={false}
          renderItem={({ item, index }) => (
            <VerticalFoodCard
              containerStyle={{
                marginLeft: index === 0 ? SIZES.padding : 18,
                marginRight: index === popular.length - 1 ? SIZES.padding : 0,
              }}
              item={item}
              onPress={() => {
                console.log('Vertical Food Card');
                navigation.navigate('FoodDetail', { food: item });
              }}
            />
          )}
        />
      </Section>
    );
  }

  function renderMenuType() {
    return (
      <FlatList
        horizontal
        data={menu}
        keyExtractor={(item) => `${item.id}`}
        showHorizontalScrollIndicator={false}
        contentContainerStyle={{
          marginTop: 30,
          marginBottom: 20,
        }}
        renderItem={({ item, index }) => (
          <TouchableOpacity
            style={{
              marginLeft: SIZES.padding,
              marginRight: index === menu.length - 1 ? SIZES.padding : 0,
            }}
            onPress={() => {
              setSelectedMenuType(item.id);
              handleChangeCategory(selectedCategoryId, item.id);
            }}
          >
            <Text
              style={{
                color:
                  selectedMenuType === item.id ? COLORS.primary : COLORS.black,
                ...FONTS.h3,
              }}
            >
              {item.name}
            </Text>
          </TouchableOpacity>
        )}
      />
    );
  }

  return (
    <View
      style={{
        flex: 1,
      }}
    >
      {/* Search */}
      {renderSearch()}

      {/* Filter */}
      {showFilterModal && (
        <FilterModal
          isVisible={showFilterModal}
          onClose={() => {
            setShowFilterModal(false);
          }}
        />
      )}

      {/* List */}
      <FlatList
        data={menuList}
        keyExtractor={(item) => `${item.id}`}
        showsVerticalScrollIndicator={false}
        ListHeaderComponent={
          <View>
            {/* Delivery To */}
            {renderDeliveryTo()}

            {/* Food Categories */}
            {renderFoodCategories()}

            {/* Popular */}
            {renderPopularSection()}

            {/* Recommended */}
            {renderRecommendedSection()}

            {/* Menu Type */}
            {renderMenuType()}
          </View>
        }
        renderItem={({ item, index }) => {
          return (
            <HorizontalFoodCard
              containerStyle={{
                height: 130,
                alignItems: 'center',
                marginHorizontal: SIZES.padding,
                marginBottom: SIZES.radius,
              }}
              imageStyle={{
                marginLeft: SIZES.base,
                borderRadius: SIZES.radius,
                height: 110,
                width: 110,
              }}
              item={item}
              onPress={() => {
                console.log('HorizontalFoodCard');
                navigation.navigate('FoodDetail', { food: item });
              }}
            />
          );
        }}
        ListFooterComponent={<View style={{ height: 200 }} />}
      />
    </View>
  );
};

function mapStateToProps(state) {
  const {
    allProducts,
    latestProducts,
    topRatingProducts,
    topDiscountProducts,
  } = state.productReducer;
  return {
    categories: state.categoryReducer?.data,
    allProducts,
    latestProducts,
    topRatingProducts,
    topDiscountProducts,
    user: state.userReducer?.user,
    isAuthenticated: state.userReducer?.isAuthenticated,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    getCategories: () => {
      return dispatch(getCategories());
    },
    getAllProducts: () => {
      return dispatch(getAllProducts());
    },
    getLatestProducts: () => {
      return dispatch(getLatestProducts());
    },
    getTopRatingProducts: () => {
      return dispatch(getTopRatingProducts());
    },
    getTopDiscountProducts: () => {
      return dispatch(getTopDiscountProducts());
    },
    handleLoadUser: () => {
      return dispatch(handleLoadUser());
    },
    loadCart: () => {
      return dispatch(loadCart());
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
