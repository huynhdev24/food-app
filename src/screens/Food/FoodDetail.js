import { htmlToText } from 'html-to-text';
import React, { useState } from 'react';
import { Image, Platform, ScrollView, Text, View } from 'react-native';
import { connect } from 'react-redux';
import {
  CartQuantityButton,
  Header,
  IconButton,
  IconLabel,
  Rating,
  StepperInput,
  TextButton,
} from '../../components';
import LineDivider from '../../components/LineDivider';
import { COLORS, FONTS, icons, images, SIZES } from '../../constants';
import { addCart } from '../../stores/cart/cartActions';
import { utils } from '../../utils';

const FoodDetail = ({ navigation, route, addCart }) => {
  const { food } = route.params;
  const [foodItem, setFoodItem] = useState(food);
  const [selectedSize, setSelectedSize] = useState('');
  const [selectedColor, setSelectedColor] = useState('');
  const [quantity, setQuantity] = useState(1);

  function renderHeader() {
    return (
      <Header
        title="CHI TIẾT MÓN ĂN"
        containerStyle={{
          height: 50,
          marginHorizontal: SIZES.padding,
          marginTop: Platform.OS === 'ios' ? 40 : 20,
        }}
        leftComponent={
          <IconButton
            icon={icons.back}
            containerStyle={{
              width: 40,
              height: 40,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 1,
              borderRadius: SIZES.radius,
              borderColor: COLORS.gray2,
            }}
            iconStyle={{
              width: 20,
              height: 20,
              tintColor: COLORS.gray2,
            }}
            onPress={() => {
              navigation.goBack();
            }}
          />
        }
        rightComponent={
          <CartQuantityButton
            quantity={3}
            onPress={() => {
              navigation.navigate('MyCart');
            }}
          />
        }
      />
    );
  }

  function renderDetails() {
    const textReview = htmlToText(foodItem?.review, {
      wordwrap: 10000,
    });

    return (
      <View
        style={{
          marginTop: SIZES.radius,
          marginBottom: SIZES.padding,
          paddingHorizontal: SIZES.padding,
        }}
      >
        {/* Food Card */}
        <View
          style={{
            height: 180,
            borderRadius: 15,
            backgroundColor: COLORS.lightGray2,
          }}
        >
          {/* Calories & Favorite */}
          {/* <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: SIZES.base,
              paddingHorizontal: SIZES.radius,
            }}
          >
            <View style={{ flexDirection: 'row' }}>
              <Image
                source={icons.calories}
                style={{
                  width: 30,
                  height: 30,
                }}
              />

              <Text style={{ color: COLORS.darkGray2, ...FONTS.body4 }}>
                {foodItem?.calories} calories
              </Text>
            </View>

            <Image
              source={icons.love}
              style={{
                width: 20,
                height: 20,
                tintColor: foodItem?.isFavorite ? COLORS.primary : COLORS.gray,
              }}
            />
          </View> */}

          {/* Food Image */}
          <Image
            source={{ uri: foodItem?.images[0]?.imageUrl }}
            resizeMode="cover"
            style={{
              height: '100%',
              width: '100%',
              borderRadius: SIZES.radius,
            }}
          />
        </View>

        {/* Food Info */}
        <View style={{ marginTop: SIZES.padding }}>
          {/* Name & Description */}
          <Text style={{ ...FONTS.h1 }}>{foodItem?.name}</Text>
          <Text
            style={{
              marginTop: SIZES.base,
              color: COLORS.darkGray,
              textAlign: 'justify',
              ...FONTS.body3,
            }}
          >
            {foodItem?.description}
          </Text>
          {/* Ratings, Duration & Shipping */}
          <View
            style={{
              flexDirection: 'row',
              marginTop: SIZES.padding,
            }}
          >
            {/* Ratings */}
            <IconLabel
              containerStyle={{
                backgroundColor: COLORS.primary,
              }}
              icon={icons.star}
              label={foodItem?.rating?.toFixed(1)}
              labelStyle={{ color: COLORS.white }}
            />

            {/* Duration */}
            {/* <IconLabel
              containerStyle={{
                marginLeft: SIZES.radius,
                paddingHorizontal: 0,
              }}
              icon={icons.clock}
              iconStyle={{ tintColor: COLORS.black }}
              label="30 phút"
            /> */}

            {/* Shipping */}
            <IconLabel
              containerStyle={{
                marginLeft: SIZES.radius,
                paddingHorizontal: 0,
              }}
              icon={icons.dollar}
              iconStyle={{ tintColor: COLORS.black }}
              label="Miễn phí giao hàng"
            />
          </View>
          {/* Sizes */}
          {foodItem?.sizes?.length > 0 && (
            <View
              style={{
                flexDirection: 'row',
                marginTop: SIZES.padding,
                alignItems: 'center',
              }}
            >
              <Text style={{ ...FONTS.h3 }}>Kích thước: </Text>

              <View
                style={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  marginLeft: SIZES.padding,
                }}
              >
                {foodItem.sizes.map((item, index) => {
                  return (
                    <TextButton
                      key={`Sizes-${index}`}
                      buttonContainerStyle={{
                        width: 30,
                        height: 30,
                        margin: SIZES.base,
                        borderWidth: 1,
                        borderRadius: SIZES.radius,
                        borderColor:
                          selectedSize === item ? COLORS.primary : COLORS.gray2,
                        backgroundColor:
                          selectedSize === item ? COLORS.primary : null,
                      }}
                      label={item}
                      labelStyle={{
                        color:
                          selectedSize === item ? COLORS.white : COLORS.gray2,
                        ...FONTS.body2,
                      }}
                      onPress={() => {
                        setSelectedSize(item);
                      }}
                    />
                  );
                })}
              </View>
            </View>
          )}
          {/* Colors */}
          {foodItem?.sizes?.length > 0 && (
            <View
              style={{
                flexDirection: 'row',
                marginTop: 5,
                alignItems: 'center',
              }}
            >
              <Text style={{ ...FONTS.h3 }}>Màu sắc: </Text>

              <View
                style={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  marginLeft: SIZES.padding,
                }}
              >
                {foodItem.colors.map((item, index) => {
                  return (
                    <TextButton
                      key={`Colors-${index}`}
                      buttonContainerStyle={{
                        // width: 55,
                        paddingHorizontal: 10,
                        height: 30,
                        margin: SIZES.base,
                        borderWidth: 1,
                        borderRadius: SIZES.radius,
                        borderColor:
                          selectedColor === item
                            ? COLORS.primary
                            : COLORS.gray2,
                        backgroundColor:
                          selectedColor === item ? COLORS.primary : null,
                      }}
                      label={item}
                      labelStyle={{
                        color:
                          selectedColor === item ? COLORS.white : COLORS.gray2,
                        ...FONTS.body2,
                      }}
                      onPress={() => {
                        setSelectedColor(item);
                      }}
                    />
                  );
                })}
              </View>
            </View>
          )}

          {/* Discount */}
          <View
            style={{
              flexDirection: 'row',
              marginTop: SIZES.radius,
              alignItems: 'center',
            }}
          >
            <Text style={{ ...FONTS.h3 }}>Giảm giá: </Text>

            <Text
              style={{
                color: COLORS.darkGray,
                marginLeft: SIZES.padding,
                ...FONTS.body3,
              }}
            >
              {<>{utils.convertPrice(foodItem?.discount)}&#8363;</>}
            </Text>
          </View>

          {/* Quantity */}
          <View
            style={{
              flexDirection: 'row',
              marginTop: SIZES.padding,
              alignItems: 'center',
            }}
          >
            <Text style={{ ...FONTS.h3 }}>Còn lại: </Text>

            <Text
              style={{
                color: COLORS.darkGray,
                marginLeft: SIZES.padding,
                ...FONTS.body3,
              }}
            >
              {utils.convertPrice(foodItem?.quantity)}
            </Text>
          </View>

          {/* Unit */}
          <View
            style={{
              flexDirection: 'row',
              marginTop: SIZES.padding,
              alignItems: 'center',
            }}
          >
            <Text style={{ ...FONTS.h3 }}>Đơn vị tính: </Text>

            <Text
              style={{
                color: COLORS.darkGray,
                marginLeft: SIZES.padding,
                ...FONTS.body3,
              }}
            >
              {foodItem?.unit}
            </Text>
          </View>

          {/* Review */}
          <View
            style={{
              marginTop: SIZES.padding,
            }}
          >
            <Text style={{ ...FONTS.h3 }}>Mô tả chi tiết: </Text>

            <Text
              style={{
                color: COLORS.darkGray,
                marginLeft: SIZES.padding,
                ...FONTS.body3,
              }}
            >
              {textReview}
            </Text>
          </View>
        </View>
      </View>
    );
  }

  function renderRestaurant() {
    return (
      <View
        style={{
          flexDirection: 'row',
          marginVertical: SIZES.padding,
          paddingHorizontal: SIZES.padding,
          alignItems: 'center',
        }}
      >
        <Image
          source={images.profile2}
          style={{
            width: 50,
            height: 50,
            borderRadius: SIZES.radius,
          }}
        />

        {/* Info */}
        <View
          style={{
            flex: 1,
            marginLeft: SIZES.radius,
            justifyContent: 'center',
          }}
        >
          <Text style={{ ...FONTS.h3 }}>Yum Yum</Text>
          <Text style={{ color: COLORS.gray, ...FONTS.body4 }}>
            1.2 KM away from you
          </Text>
        </View>

        {/* Ratings */}
        <Rating rating={4} iconStyle={{ marginLeft: 3 }} />
      </View>
    );
  }

  function renderFooter() {
    return (
      <View
        style={{
          flexDirection: 'row',
          height: 120,
          alignItems: 'center',
          paddingHorizontal: SIZES.padding,
          paddingBottom: SIZES.radius,
        }}
      >
        {/* Stepper Input */}
        <StepperInput
          value={quantity}
          onAdd={() => {
            setQuantity(quantity + 1);
          }}
          onMinus={() => {
            if (quantity > 1) {
              setQuantity(quantity - 1);
            }
          }}
        />

        {/* Text Button */}
        <TextButton
          buttonContainerStyle={{
            flex: 1,
            flexDirection: 'row',
            height: 60,
            marginLeft: SIZES.radius,
            borderRadius: SIZES.radius,
            paddingHorizontal: SIZES.radius,
            backgroundColor: COLORS.primary,
          }}
          label="Mua ngay"
          label2={<>&#8363;{utils.convertPrice(foodItem?.price)}</>}
          onPress={() => {
            if (foodItem?.quantity > 0) {
              addCart(
                { ...foodItem, sizes: [selectedSize], colors: [selectedColor] },
                quantity
              );

              navigation.navigate('MyCart');
            }
          }}
        />
      </View>
    );
  }

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: COLORS.white,
      }}
    >
      {/* Header */}
      {renderHeader()}

      {/* Body */}
      <ScrollView>
        {/* Food Detail */}
        {renderDetails()}

        {/* <LineDivider /> */}

        {/* Restaurant */}
        {/* {renderRestaurant()} */}
      </ScrollView>

      {/* Footer */}
      <LineDivider />
      {renderFooter()}
    </View>
  );
};

function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  return {
    addCart: (product, newQuantity) => {
      return dispatch(addCart(product, newQuantity));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FoodDetail);
