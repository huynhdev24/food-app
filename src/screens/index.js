import MainLayout from './MainLayout';

// Home
import Home from './Home/Home';
import FilterModal from './Home/FilterModal';

// Search
import Search from './Search/Search';

// Cart
import CartTab from './Cart/CartTab';

// Favorite
import Favorite from './Favorite/Favorite';

// Notification
import Notification from './Notification/Notification';

import OnBoarding from './OnBoarding/OnBoarding';

// Authentication
import SignIn from './Authentication/SignIn';
import SignUp from './Authentication/SignUp';
import Otp from './Authentication/Otp';
import ForgotPassword from './Authentication/ForgotPassword';
import AuthLayout from './Authentication/AuthLayout';

// Food
import FoodDetail from './Food/FoodDetail';

// Cart
import Checkout from './Cart/Checkout';
import MyCart from './Cart/MyCart';
import Success from './Cart/Success';

// Card
import AddCard from './Card/AddCard';
import MyCard from './Card/MyCard';

// Delivery
import DeliveryStatus from './Delivery/DeliveryStatus';
import Map from './Delivery/Map';

export {
  MainLayout,
  Home,
  Search,
  CartTab,
  Favorite,
  Notification,
  FilterModal,
  OnBoarding,
  SignIn,
  SignUp,
  Otp,
  ForgotPassword,
  AuthLayout,
  FoodDetail,
  Checkout,
  MyCart,
  Success,
  AddCard,
  MyCard,
  DeliveryStatus,
  Map,
};
