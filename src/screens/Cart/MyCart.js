import React, { useEffect, useState } from 'react';
import { Image, Platform, StyleSheet, Text, View } from 'react-native';
import { SwipeListView } from 'react-native-swipe-list-view';
import { connect } from 'react-redux';
import {
  CartQuantityButton,
  FooterTotal,
  Header,
  IconButton,
  StepperInput,
} from '../../components';
import { COLORS, dummyData, FONTS, icons, SIZES } from '../../constants';
import { deleteCartItem, updateCart } from '../../stores/cart/cartActions';
import { utils } from '../../utils';

const MyCart = ({
  navigation,
  headerStyle,
  buttonContainerStyle,
  cart,
  updateCart,
  allProducts,
  deleteCartItem,
}) => {
  const [total, setTotal] = useState(0);

  // Handler
  const onClickInc = (index) => {
    const product = allProducts.find(
      (dataItem) => dataItem._id === cart[index]?.product?.id
    );
    updateCart(index, cart[index]?.quantity + 1, product);
  };

  const onClickDec = (index, quantity) => {
    const product = allProducts.find(
      (dataItem) => dataItem._id === cart[index]?.product?.id
    );
    if (quantity > 1) {
      cart[index].quantity -= 1;
    } else {
      cart[index].quantity = 1;
    }
    updateCart(index, cart[index].quantity, product);
  };

  useEffect(() => {
    if (cart) {
      let sumTotal = 0;
      cart.forEach((item, index) => {
        sumTotal += item.total;
        const product = allProducts.find(
          (dataItem) => dataItem._id === item?.product?.id
        );
        if (parseInt(item.quantity) > parseInt(product?.quantity)) {
          updateCart(index, product.quantity, product);
        }
      });
      setTotal(sumTotal);
    }
  }, [cart, allProducts, updateCart]);

  // Render
  function renderHeader() {
    return (
      <Header
        title="GIỎ HÀNG CỦA TÔI"
        containerStyle={{
          height: 50,
          marginHorizontal: SIZES.padding,
          marginTop: Platform.OS === 'ios' ? 40 : 20,
          ...headerStyle,
        }}
        leftComponent={
          <IconButton
            icon={icons.back}
            containerStyle={{
              width: 40,
              height: 40,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 1,
              borderRadius: SIZES.radius,
              borderColor: COLORS.gray2,
            }}
            iconStyle={{
              width: 20,
              height: 20,
              tintColor: COLORS.gray2,
            }}
            onPress={() => {
              navigation.goBack();
            }}
          />
        }
        rightComponent={<CartQuantityButton quantity={3} />}
      />
    );
  }

  function renderCartList() {
    return (
      cart?.length > 0 && (
        <SwipeListView
          data={cart}
          keyExtractor={(item) => `${item?.product?.id}`}
          contentContainerStyle={{
            marginTop: SIZES.radius,
            paddingHorizontal: SIZES.padding,
            paddingBottom: SIZES.padding * 2,
          }}
          disableRightSwipe={true}
          rightOpenValue={-75}
          renderItem={(data, rowMap) => {
            return (
              <View
                style={{
                  height: 100,
                  backgroundColor: COLORS.lightGray2,
                  ...styles.cartItemContainer,
                }}
              >
                {/* Food Image */}
                <View
                  style={{
                    width: 90,
                    height: 100,
                    marginLeft: -10,
                  }}
                >
                  <Image
                    source={{ uri: data?.item.product?.images[0].imageUrl }}
                    resizeMode="contain"
                    style={{
                      width: '100%',
                      height: '100%',
                      position: 'absolute',
                      borderRadius: SIZES.radius,
                    }}
                  />
                </View>

                {/* Food Info */}
                <View style={{ flex: 1, marginLeft: SIZES.base }}>
                  <Text style={{ ...FONTS.body3 }}>
                    {data?.item.product?.name}
                  </Text>
                  <Text style={{ color: COLORS.primary, ...FONTS.h3 }}>
                    {utils.convertPrice(data?.item.product?.price)}
                  </Text>
                </View>

                {/* Quantity */}
                <StepperInput
                  containerStyle={{
                    height: 50,
                    width: 125,
                    backgroundColor: COLORS.white,
                  }}
                  value={data?.item?.quantity}
                  onAdd={() => {
                    onClickInc(data.index);
                  }}
                  onMinus={() => {
                    if (data?.item?.quantity > 1) {
                      onClickDec(data.index, data?.item?.quantity);
                    }
                  }}
                />
              </View>
            );
          }}
          renderHiddenItem={(data, rowMap) => (
            <IconButton
              containerStyle={{
                flex: 1,
                justifyContent: 'flex-end',
                backgroundColor: COLORS.primary,
                ...styles.cartItemContainer,
              }}
              icon={icons.delete_icon}
              iconStyle={{
                marginRight: 10,
              }}
              onPress={() => {
                // removeMyCartHandler(data.item.id)
                deleteCartItem(data.item.product);
              }}
            />
          )}
        />
      )
    );
  }

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: COLORS.white,
      }}
    >
      {/* Header */}
      {renderHeader()}

      {/* Cart List */}
      {renderCartList()}

      {/* Footer */}
      <FooterTotal
        subTotal={total}
        shippingFee={0.0}
        total={total}
        onPress={() => {
          navigation.navigate('Checkout');
        }}
        buttonContainerStyle={buttonContainerStyle}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  cartItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: SIZES.radius,
    paddingHorizontal: SIZES.radius,
    borderRadius: SIZES.radius,
  },
});

function mapStateToProps(state) {
  return {
    cart: state.cartReducer.cart,
    allProducts: state.productReducer.allProducts,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    updateCart: (index, newQuantity, product) => {
      return dispatch(updateCart(index, newQuantity, product));
    },
    deleteCartItem: (product) => {
      return dispatch(deleteCartItem(product));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MyCart);
