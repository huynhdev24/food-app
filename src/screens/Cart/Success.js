import React, { useEffect } from 'react';
import { View, Text, BackHandler, Image } from 'react-native';
import { TextButton, TextIconButton } from '../../components';
import { COLORS, FONTS, icons, images, SIZES } from '../../constants';

const Success = ({ navigation }) => {
  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      () => {
        return true;
      }
    );

    return () => {
      backHandler.remove();
    };
  }, []);
  return (
    <View
      style={{
        flex: 1,
        paddingHorizontal: SIZES.padding,
        backgroundColor: COLORS.white,
      }}
    >
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Image
          source={images.success}
          resizeMode="contain"
          style={{
            width: 150,
            height: 150,
          }}
        />

        <Text style={{ marginTop: SIZES.padding, ...FONTS.h1 }}>
          Đặt hàng thành công!
        </Text>
        <Text
          style={{
            textAlign: 'center',
            marginTop: SIZES.base,
            color: COLORS.darkGray,
            ...FONTS.body3,
          }}
        >
          Đơn hàng sẽ được giao đến bạn sau vài phút nữa
        </Text>
      </View>

      {/* TextIconButton */}
      <TextButton
        label="Xem bản đồ"
        buttonContainerStyle={{
          height: 55,
          marginBottom: SIZES.padding,
          borderRadius: SIZES.radius,
          backgroundColor: COLORS.primary,
        }}
        onPress={() => {
          navigation.replace('Map');
        }}
        icon={icons.map}
        iconPosition="LEFT"
      />
    </View>
  );
};

export default Success;
