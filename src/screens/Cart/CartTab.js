import React from 'react';
import { Text, View } from 'react-native';
import MyCart from './MyCart';

const CartTab = () => {
  return (
    // <View
    //   style={{
    //     flex: 1,
    //     alignItems: 'center',
    //     justifyContent: 'center',
    //   }}
    // >
    //   <Text>CartTab</Text>
    // </View>
    <MyCart
      headerStyle={{
        display: 'none',
      }}
      buttonContainerStyle={{
        marginBottom: 180,
      }}
    />
  );
};

export default CartTab;
