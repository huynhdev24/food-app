import React, { useState } from 'react';
import { Image, Platform, Text, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import {
  CardItem,
  FooterTotal,
  FormInput,
  Header,
  IconButton,
} from '../../components';
import { COLORS, dummyData, FONTS, icons, SIZES } from '../../constants';
import { createOrder } from '../../services/orders';
import { deleteCart } from '../../stores/cart/cartActions';

const Checkout = ({ navigation, deleteCart, user, cart }) => {
  const [selectedCard, setSelectedCard] = useState(dummyData.myCards[1]);

  function renderHeader() {
    return (
      <Header
        title="THANH TOÁN"
        containerStyle={{
          height: 50,
          marginHorizontal: SIZES.padding,
          marginTop: Platform.OS === 'ios' ? 40 : 20,
        }}
        leftComponent={
          <IconButton
            icon={icons.back}
            containerStyle={{
              width: 40,
              height: 40,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 1,
              borderRadius: SIZES.radius,
              borderColor: COLORS.gray2,
            }}
            iconStyle={{
              width: 20,
              height: 20,
              tintColor: COLORS.gray2,
            }}
            onPress={() => {
              navigation.goBack();
            }}
          />
        }
        rightComponent={<View style={{ width: 40 }} />}
      />
    );
  }

  function renderMyCards() {
    return (
      <View>
        {dummyData.myCards.map((item, index) => {
          return (
            <CardItem
              key={`MyCard-${item.id}`}
              item={item}
              isSelected={
                `${selectedCard?.key || 'MyCard'}-${selectedCard?.id || 2}` ===
                `MyCard-${item.id}`
              }
              onPress={() => {
                setSelectedCard({ ...item, key: 'MyCard' });
              }}
            />
          );
        })}
      </View>
    );
  }

  function renderDeliveryAddress() {
    return (
      <View
        style={{
          marginTop: SIZES.padding,
        }}
      >
        <Text
          style={{
            ...FONTS.h3,
          }}
        >
          Địa chỉ nhận hàng
        </Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: SIZES.radius,
            paddingVertical: SIZES.radius,
            paddingHorizontal: SIZES.padding,
            borderWidth: 2,
            borderRadius: SIZES.radius,
            borderColor: COLORS.lightGray2,
          }}
        >
          <Image
            source={icons.location1}
            style={{
              width: 40,
              height: 40,
            }}
          />

          <Text
            style={{
              marginLeft: SIZES.radius,
              width: '85%',
              ...FONTS.body3,
            }}
          >
            {user?.address}
          </Text>
        </View>
      </View>
    );
  }

  function renderCoupon() {
    return (
      <View
        style={{
          marginTop: SIZES.padding,
        }}
      >
        <Text style={{ ...FONTS.h3 }}>Add Coupon</Text>

        <FormInput
          containerStyle={{
            marginTop: 0,
            paddingLeft: SIZES.padding,
            paddingBottom: SIZES.padding,
            paddingRight: 0,
            borderWidth: 2,
            borderColor: COLORS.lightGray2,
            backgroundColor: COLORS.white,
            overflow: 'hidden',
          }}
          placeholder="Mã giảm giá"
          appendComponent={
            <View
              style={{
                width: 60,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: COLORS.primary,
              }}
            >
              <Image
                source={icons.discount}
                style={{ width: 40, height: 40 }}
              />
            </View>
          }
        />
      </View>
    );
  }

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: COLORS.white,
      }}
    >
      {/* Header */}
      {renderHeader()}

      {/* Body */}
      <KeyboardAwareScrollView
        keyboardDismissMode="on-drag"
        extraScrollHeight={-200}
        contentContainerStyle={{
          flexGrow: 1,
          paddingHorizontal: SIZES.padding,
          paddingBottom: 20,
        }}
      >
        {/* My Cards */}
        {renderMyCards()}

        {/* Delivery Address */}
        {renderDeliveryAddress()}

        {/* Coupon */}
        {/* {renderCoupon()} */}
      </KeyboardAwareScrollView>

      <FooterTotal
        subTotal={37.97}
        shippingFee={0.0}
        total={37.97}
        onPress={async () => {
          if (!user?.address) {
            const orderDetail = [];
            cart.forEach((item) => {
              orderDetail.push({
                id: item.product.id,
                quantity: item.quantity,
                color: item.product.colors[0],
                size: item.product.sizes[0],
              });
            });
            const response = await createOrder({
              orderDetail,
              note: '',
              deliveryFee: 0,
              isUseCoin: false,
              deliveryMethod: 'YUM_YUM_EXPRESS',
            });
            if (response.status === 200) {
              deleteCart();
              navigation.replace('Success');
            }
          }
        }}
      />
    </View>
  );
};

function mapStateToProps(state) {
  return {
    user: state.userReducer.user,
    cart: state.cartReducer.cart,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    deleteCart: () => {
      return dispatch(deleteCart());
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Checkout);
