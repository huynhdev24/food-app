import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React, { useEffect } from 'react';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import CustomDrawer from './navigation/CustomDrawer';
import rootReducer from './stores/rootReducer';
import {
  SignIn,
  FoodDetail,
  Checkout,
  MyCart,
  Success,
  Map,
  // OnBoarding,
  // SignUp,
  // ForgotPassword,
  // Otp,
  // AddCard,
  // MyCard,
  // DeliveryStatus,
} from './screens';
import SplashScreen from 'react-native-splash-screen';
import EditProfile from './screens/Profile/EditProfile';

const Stack = createNativeStackNavigator();

const store = createStore(rootReducer, applyMiddleware(thunk));

const App = () => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
          initialRouteName={'Home'}
        >
          <Stack.Screen name="Home" component={CustomDrawer} />
          <Stack.Screen name="SignIn" component={SignIn} />
          <Stack.Screen name="FoodDetail" component={FoodDetail} />
          <Stack.Screen name="Checkout" component={Checkout} />
          <Stack.Screen name="MyCart" component={MyCart} />
          <Stack.Screen name="Success" component={Success} />
          <Stack.Screen name="Map" component={Map} />
          <Stack.Screen name="EditProfile" component={EditProfile} />
          {/* <Stack.Screen name="OnBoarding" component={OnBoarding} /> */}
          {/* <Stack.Screen name="SignUp" component={SignUp} /> */}
          {/* <Stack.Screen name="ForgotPassword" component={ForgotPassword} /> */}
          {/* <Stack.Screen name="Otp" component={Otp} /> */}
          {/* <Stack.Screen name="AddCard" component={AddCard} /> */}
          {/* <Stack.Screen name="MyCard" component={MyCard} /> */}
          {/* <Stack.Screen name="DeliveryStatus" component={DeliveryStatus} /> */}
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
